import { APIGatewayEvent, APIGatewayProxyResult, Context } from "aws-lambda";

exports.handler = async (
  event: APIGatewayEvent,
  context: Context
): Promise<APIGatewayProxyResult> => {
  return {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
    body: JSON.stringify({
      message: "Knock, Knock, Knock, Penny...",
    }),
  };
};
